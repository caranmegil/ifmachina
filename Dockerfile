FROM node:18-alpine3.14
RUN apk update && apk upgrade && sync
EXPOSE 3000
WORKDIR /app/ifmachina
COPY . /app/ifmachina
RUN npm ci
ENV HOST=0.0.0.0
EXPOSE 3000
CMD ["npm", "start"]