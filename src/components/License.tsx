/*
Copyright (C) 2022  William Moore <william@nerderium.com>

This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of 
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License along with this program.  If not, see 
<http://www.gnu.org/licenses/>.
*/

import React from "react";

const License = (props: any) => {
  return (
    <div>
      Interactive Fiction Machina is licensed through <a href="https://codeberg.org/caranmegil/ifmachina/src/branch/main/LICENSE" target="_blank" rel="noreferrer">AGPL</a>.
      <p>
        The rest of the components are:
        <ul>
          <li>Quixe is copyright (c) 2010-2020, Andrew Plotkin (<a href="https://github.com/erkyrath/lectrote/blob/master/LICENSE" target="_blank" rel="noreferrer">MIT license</a>)</li>
          <li>inkjs is copyright (c) 2017-2020 Yannick Lohse (<a href="https://github.com/erkyrath/lectrote/blob/master/LICENSE" target="_blank" rel="noreferrer">MIT license</a>)</li>
          <li>ifvms.js is copyright (c) 2016 Dannii Willis and other contributors (<a href="https://github.com/erkyrath/lectrote/blob/master/LICENSE" target="_blank" rel="noreferrer">MIT license</a></li>
          <li>emglken is copyright (c) 2012-2017, Andrew Plotkin, Dannii Willis (<a href="https://github.com/erkyrath/lectrote/blob/master/LICENSE" target="_blank" rel="noreferrer">MIT license</a>)</li>
          <li>Git (in emglken) is copyright (c) 2003 Iain Merrick (<a href="https://github.com/erkyrath/lectrote/blob/master/LICENSE" target="_blank" rel="noreferrer">MIT license</a>)</li>
          <li>Glulxe (in emglken) is copyright (c) 1999-2016, Andrew Plotkin (<a href="https://github.com/erkyrath/lectrote/blob/master/LICENSE" target="_blank" rel="noreferrer">MIT license</a>)</li>
          <li>Hugo (in emglken) is copyright (c) 2011 by Kent Tessman (<a href="https://github.com/erkyrath/lectrote/blob/master/LICENSE" target="_blank" rel="noreferrer">BSD license</a>)</li>
          <li>TADS (in emglken) is copyright (c) 1991-2012 by Michael J. Roberts (<a href="https://github.com/erkyrath/lectrote/blob/master/LICENSE" target="_blank" rel="noreferrer">dual-licensed GPL/TADS license</a>)</li>
          <li>RemGlk (in emglken) is copyright (c) 2012-2020, Andrew Plotkin (<a href="https://github.com/erkyrath/lectrote/blob/master/LICENSE" target="_blank" rel="noreferrer">MIT license</a>)</li>
        </ul>
      </p>
    </div>
  );
};

export default License;