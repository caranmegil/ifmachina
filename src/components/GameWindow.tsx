/*
Copyright (C) 2022  William Moore <william@nerderium.com>

This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License
as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License along with this program.  If not, see
<http://www.gnu.org/licenses/>.
*/

import React, { useEffect, useRef, useState } from "react";
import { Link, useParams } from "react-router-dom";
import Blorb from "../upstream/asyncglk/src/blorb/blorb";
import { FileView } from "../upstream/asyncglk/src/blorb/iff";
import get_default_options, { ParchmentOptions } from "../common/options";
import { fetch_storyfile, fetch_vm_resource } from "../common/file";
import { formats, identify_blorb_storyfile_format } from "../common/formats.js";
import { getCollectedBlobNames, getCollectedBlobDataByName, isLoggedIn } from "../arcade-store";

import '../upstream/asyncglk/src/glkote/web/light.css';
import '../upstream/asyncglk/src/glkote/web/glkote.css';
import '../upstream/asyncglk/src/glkote/web/core.css';
import '../upstream/asyncglk/src/glkote/web/windows.css';
import '../assets/parchment.css';
import '../upstream/glkote/dialog.css';
import '../assets/fonts/fonts.css';
import '../upstream/asyncglk/src/glkote/web/errors.css';
import '../upstream/asyncglk/src/glkote/web/input.css';
import '../upstream/asyncglk/src/glkote/web/styles.css';

const GameWindow = () => {
  const baseOptionsRef = useRef<ParchmentOptions | null>(null);
  const [isSignedIn, setIsSignedIn] = useState(false);
  const { fileName } = useParams();

  function find_format(format: any, path: any) {
    for (const formatspec of formats) {
      if (formatspec.id === format || formatspec.extensions.test(path)) {
        return formatspec;
      }
    }
    throw new Error("Unknown storyfile format");
  }

  function query_options() {
    // Some options can be specified in the URL query
    const query = new URLSearchParams(document.location.search);
    const options: any = {};
    const query_options = ["do_vm_autosave", "story"];
    for (const option of query_options) {
      if (query.has(option)) {
        options[option] = query.get(option);
      }
    }
    return options;
  }

  useEffect(() => {
    if (isSignedIn) {
      async function load(story: any, format: any) {
        const baseOptions = baseOptionsRef.current;
        if (baseOptions == null) return;
        try {
          // Identify the format
          if (!format) {
            if (typeof story === "string") {
              format = find_format(null, story);
            } else {
              throw new Error("Cannot identify storyfile format without path");
            }
          }
  
          // If a blorb file extension is generic, we must download it first to identify its format
          let blorb;
          if (format.id === "blorb") {
            if (typeof story === "string") {
              story = await fetch_storyfile(baseOptions, story);
            }
            blorb = new Blorb(story);
            format = identify_blorb_storyfile_format(blorb);
          }
  
          if (typeof format === "string") {
            format = find_format(format, null);
          }
          const engine = format.engines[0];
  
          const requires = await Promise.all([
            typeof story === "string"
              ? fetch_storyfile(baseOptions, story)
              : story,
            ...engine.load.map((path: any) =>
              fetch_vm_resource(baseOptions, path)
            ),
          ]);
  
          story = requires[0];
  
          // If the story is a Blorb, then parse it and pass in the options
          const options = { ...baseOptions };
          const view = new FileView(story);
          if (view.getFourCC(0) === "FORM" && view.getFourCC(8) === "IFRS") {
            options.Blorb = blorb || new Blorb(story);
          }
  
          await engine.start(options, requires);
        } catch (err) {
          baseOptions.GlkOte.error(err);
          throw err;
        }
      }
  
      if (fileName) {
        getCollectedBlobNames((files: string[]) => {
          const dataName =
            files
              .filter((file) => file === fileName)[0] ?? null;
          if (dataName != null) {
            getCollectedBlobDataByName(dataName, (data) => {
              const format = find_format(null, fileName);
              if (data !== null && format) {
                const url = `data:application/x-${format.id};base64,${data}`;
      
                if (baseOptionsRef.current == null) {
                  const default_options = get_default_options();
                  const qoptions = query_options();
                  const baseOptions = { ...default_options, ...qoptions };
                  baseOptionsRef.current = baseOptions;
                }
      
                if (!url) {
                  return;
                }
                // Validate the URL
                try {
                  new URL(url);
                } catch (e) {
                  console.log(e);
                  return;
                }
      
                // Change the page URL
                load(url, null);
              }
            })
          }
        });
      }
    }
  }, [fileName, isSignedIn]);

  const handle = setInterval( () => {
    isLoggedIn().then( (value: boolean) => {
      setIsSignedIn(value);
    }).catch( (error) => {
      console.error(error);
    })
  }, 500);

  useEffect( () => {
    if (isSignedIn) {
      clearTimeout(handle);
    }
  }, [handle, isSignedIn])

  return (
    (isSignedIn) ?
      <div style={style}>
        <div id="gameport" style={gameportStyle}>
          <div id="windowport" style={windowportStyle}></div>
          <div id="errorpane" style={{ display: "none" }}>
            <div id="errorcontent">...</div>
          </div>
          <div id="loadingpane" style={{ display: "none" }}>
            <img src="../waiting.gif" alt="LOADING" />
            <br />
            <em>&nbsp;&nbsp;&nbsp;Loading...</em>
          </div>
        </div>
        <div style={footerStyle}>
          <a href="https://www.microheaven.com/ifguide/step3.html" target={'_blank'} rel={'noreferrer'}>A Guide on How to Play IF</a>
          <div>
            Copyright &copy; 2022 William R Moore and licensed through <Link to="/license" target="_blank">AGPL 3.0</Link>
          </div>
        </div>
      </div>
      : <div>
      <div id="loadingpane">
          <img src="../waiting.gif" alt="LOADING" />
          <br />
          <em>&nbsp;&nbsp;&nbsp;Loading...</em>
        </div>
      </div>
  );
};

const footerStyle = {
  display: 'grid',
  position: 'absolute' as 'absolute',
  alignItems: 'center',
  justifyItems: 'center',
  gridAutoRows: '1fr auto',
  gridAutoFlow: 'row',
  gap: '.75em',
  width: '100%',
  bottom: 0,
};

const windowportStyle = {
};

const gameportStyle = {
  top: "2.5em",
  height: '80vh',
}

const style = {
  display: 'grid',
  gridAutoRows: 'auto 1fr',
};

export default GameWindow;
