import React from 'react';

type InterstitialProps = {
  hasCancel?: boolean;
  title: string;
  content: JSX.Element;
  cancelFn?: () => void;
  acceptFn?: () => void;
}

const Interstitial = (props: InterstitialProps) => {
  function onCancelButton() {
    if (props.cancelFn) {
      props.cancelFn();
    }
  }

  function onAcceptButton() {
    if (props.acceptFn) {
      props.acceptFn();
    }
  }

  return (
    <div style={interstitialStyle}>
      <div>
        <h1>{props.title}</h1>
        <div>{props.content}</div>
      </div>
      <div style={footerStyle}>
        {props.hasCancel ? <button onClick={onCancelButton}>Cancel</button>
        : null
        }
        <button onClick={onAcceptButton}>{props.hasCancel ? 'Accept' : 'Okay'}</button>
      </div>
    </div>
  )
};

const interstitialStyle = {
  width: '100%',
  height: '100vh',
  display: 'grid',
  alignItems: 'center',
  position: 'absolute' as 'absolute',
  backgroundColor: 'ivory',
  textAlign: 'center' as 'center',
  margin: 'auto',
}

const footerStyle = {
  bottom: 0,
  display: 'grid',
  gridAutoFlow: 'column',
  gap: '1em',
  marginLeft: 'auto',
  marginRight: 'auto',
}

export default Interstitial;