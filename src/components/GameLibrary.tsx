/*
Copyright (C) 2022  William Moore <william@nerderium.com>

This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of 
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License along with this program.  If not, see 
<http://www.gnu.org/licenses/>.
*/

import React, {
  useEffect,
  useRef,
  useState
} from 'react';
import { Link } from 'react-router-dom';

import {
  getCollectedBlobNames,
  saveCollectedBlobFiles,
  saveCollectedBlobData,
  signIntoFirebase,
  signOutOfFirebase,
  isLoggedIn,
  deleteCollectedBlobData,
} from '../arcade-store';

import GameLibraryItem from './GameLibraryItem';
import Interstitial from './Interstital';

const GameLibrary = React.forwardRef( (props: any, ref: any) => {
  const [isSignedIn, setIsSignedIn] = useState(false);
  const inputFileRef = useRef<HTMLInputElement>(null);
  const [files, setFiles] = useState<string []>([]);

  function _arrayBufferToBase64( buffer: ArrayBuffer ) {
    var binary = '';
    var bytes = new Uint8Array( buffer );
    var len = bytes.byteLength;
    for (var i = 0; i < len; i++) {
        binary += String.fromCharCode( bytes[ i ] );
    }

    return window.btoa( binary );
  }

  async function onFileSelected(e: any) {
    if (inputFileRef.current) {
      const fileName = inputFileRef.current?.files?.[0];
      if (fileName) {
        const newFiles = files.filter(file => file !== fileName.name)
        newFiles.push(fileName.name);
        await saveCollectedBlobFiles(newFiles);
        await saveCollectedBlobData(fileName.name, _arrayBufferToBase64(await fileName.arrayBuffer()));
        getCollectedBlobNames( (files: string []) => {
          files?.sort((a: string, b: string) => {
            return a.localeCompare(b);
          });
          setFiles(files ?? []);
        });
        e.target.value = null;
      }
    }
  }

  useEffect( () => {
    isLoggedIn().then((value: boolean) => {
      setIsSignedIn(value);
      getCollectedBlobNames( (files: string []) => {
        files?.sort((a: string, b: string) => {
          console.log(a, b)
          return a.localeCompare(b);
        });
        setFiles(files ?? []);
      });
    });
  });

  const [willShowDeleteConfirm, setShowDeleteConfirm] = useState(false);
  const [deleteItemName, setDeleteItemName] = useState('');
  function confirmDeleteItem(name: string) {
    setDeleteItemName(name);
    setShowDeleteConfirm(true);
  }

  async function deleteItem(name: string) {
    const newFiles = files.filter(file => file !== name)
    await saveCollectedBlobFiles(newFiles);
    await deleteCollectedBlobData(name);
    getCollectedBlobNames( (files: string []) => {
      files?.sort((a: string, b: string) => {
        return a.localeCompare(b);
      });
      setFiles(files ?? []);
      setShowDeleteConfirm(false);
    });
  }

  async function onDelete() {
    await deleteItem(deleteItemName);
    setShowDeleteConfirm(false);
  }

  function onAccept() {
    setShowDeleteConfirm(false);
  }

  useEffect(() => {
    getCollectedBlobNames( (files: string []) => {
      files?.sort((a: string, b: string) => {
        return a.localeCompare(b);
      });
      setFiles(files ?? []);
    });

  }, [isSignedIn])

  async function onGoogleResponse() {
    if (!isSignedIn) {
      await signIntoFirebase();
      setIsSignedIn(true);
    } else {
      await signOutOfFirebase();
      setIsSignedIn(false);
    }
  }

  return (
    <div style={style}>
      {!willShowDeleteConfirm ?
      <div>
      <input ref={inputFileRef} style={{display: 'none'}}type="file" onChange={onFileSelected}></input>
      <div style={mainStyle}>
        <div style={menuBarStyle}>
          <div style={infoStyle}><a href="https://codeberg.org/caranmegil/ifmachina" target={'_blank'} rel={'noreferrer'}><strong>Interactive Fiction Machina</strong></a></div>
          { (isSignedIn) ?
            <button style={addIFStyle} onClick={()=> {
              if (inputFileRef.current) {
                inputFileRef.current.click();
              }
            }}><span className="material-icons">add</span> IF</button>
           : null }
          {isSignedIn ? <div style={menuBarSignInStyle} onClick={onGoogleResponse}>Sign Out</div> : null}
        </div>
        {isSignedIn ?
        <div className="menuWrap" style={contentStyle}>
          <ul className={'menu'}>
          { (files) ?
            files.map( (file) => <GameLibraryItem key={file} file={file} deleteItem={(name: string) => confirmDeleteItem(name)}/>)
            : null
          }
        </ul>
        </div>
        : <div style={{border: '.2em border black', margin: 'auto', borderRadius: '.5em', width: '5em'}} onClick={onGoogleResponse}><h2>Sign In</h2></div>}
        <div style={footerStyle}>
          <div>
            Copyright &copy; 2022 William R Moore and licensed through <Link to="/license" target="_blank">AGPL 3.0</Link>
          </div>
        </div>
      </div>
      </div>
      : <Interstitial title={'Delete?'} hasCancel={true} cancelFn={onAccept} acceptFn={onDelete} content={(<div>Should I delete {deleteItemName}?</div>)}/>
    
    }
    </div>
  );
});

const style = {
  backgroundColor: 'ivory',
  width: '100%',
  height: '100%',
};

const menuBarStyle = {
  display: 'grid',
  position: 'absolute' as 'absolute',
  gridAutoFlow: 'column',
  backgroundColor: 'black',
  color: 'white',
  height: '4em',
  top: 0,
  width: '100%',
  justifyItems: 'center',
  alignItems: 'center',
};

const infoStyle={
  textAlign: 'left' as 'left',
  margin: 'auto',
  width: '100%',
};

const addIFStyle = {
  borderRadius: '.3em',
  backgroundColor: 'black',
  color: 'white',
  border: '.1em solid white',
  margin: 'auto',
  alignItems: 'center',
  display: 'grid',
  gridAutoFlow: 'column',
};

const footerStyle = {
  display: 'grid',
  position: 'absolute' as 'absolute',
  alignItems: 'center',
  justifyItems: 'center',
  gridAutoRows: '1fr auto',
  gridAutoFlow: 'row',
  gap: '.75em',
  width: '100%',
  bottom: 0,
};

const menuBarSignInStyle = {
  justifySelf: 'right',
  alignSelf: 'center',
  marginRight: '2em',
};

const contentStyle = {
  top: '4em',
};

const mainStyle = {
  display: 'grid',
  alignItems: 'center',
  right: '15em',
  height: '100vh',
  width: '100%',
  gridAutoRows: '1fr auto 1fr',
};

export default GameLibrary;
