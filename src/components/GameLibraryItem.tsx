/*
Copyright (C) 2022  William Moore <william@nerderium.com>

This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of 
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License along with this program.  If not, see 
<http://www.gnu.org/licenses/>.
*/

import React from "react";
import { Link } from "react-router-dom";

const GameLibraryItem = (props: any) => {
  function deleteItem() {
    props.deleteItem(props.file)
  }

  return (
    <li className={'menu-item'}><div onClick={deleteItem}><span className="material-icons">delete</span></div><Link style={itemNameStyle} to={`/story/${encodeURI(props.file)}`} target="_blank">{props.file}</Link></li>
  )
}

const itemNameStyle = {
};

export default GameLibraryItem;