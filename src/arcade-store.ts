/*
Copyright (C) 2022  William Moore <william@nerderium.com>

This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of 
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License along with this program.  If not, see 
<http://www.gnu.org/licenses/>.
*/

import { initializeApp } from "firebase/app";
import { getDatabase, ref, set, get } from "firebase/database";
import {
    getAuth,
    GoogleAuthProvider,
    signInWithPopup,
    signOut,
} from "firebase/auth";

const firebaseConfig = {
    "apiKey": process.env.REACT_APP_apiKey,
    "authDomain": process.env.REACT_APP_authDomain,
    "databaseURL": process.env.REACT_APP_databaseURL,
    "projectId": process.env.REACT_APP_projectId,
    "storageBucket": process.env.REACT_APP_storageBucket,
    "messagingSenderId": process.env.REACT_APP_messagingSenderId,
    "appId": process.env.REACT_APP_appId,
    "measurementId": process.env.REACT_APP_measurementId
};

const app = initializeApp(firebaseConfig);
const db = getDatabase(app);

const provider = new GoogleAuthProvider();

export interface FileBlob {
  name: string;
  data: string;
}

export async function isLoggedIn() {
  const auth = getAuth();
  return auth.currentUser != null;
}

export async function signOutOfFirebase() {
  const auth = getAuth();
  await signOut(auth);
}

export async function signIntoFirebase() {
  const auth = getAuth();
  await signInWithPopup(auth, provider);
}

export async function saveCollectedBlobFiles(files: any) {
  try {
    const auth = getAuth();
    const user = auth.currentUser;
    if (user != null) {
        const collectionNameUserRef = ref(db, `/files/${user.uid}`);
        await set(collectionNameUserRef, files);
    } else {
        console.error('user is ', user);
    }
  } catch (e) {
      console.error(e);
  }
}

export async function saveCollectedBlobData(collectionName: string, data: string) {
  try {
    const cleansedName = collectionName.replace(/[\.\#\$\[\]]/g, '');
    const auth = getAuth();
    const user = auth.currentUser;
    if (user != null) {
        const collectionNameUserRef = ref(db, `/data/${user.uid}/${cleansedName}`);
        await set(collectionNameUserRef, data);
    } else {
        console.error('user is ', user);
    }
  } catch (e) {
      console.error(e);
  }
}

export async function deleteCollectedBlobData(collectionName: string) {
  try {
    const cleansedName = collectionName.replace(/[\.\#\$\[\]]/g, '');
    const auth = getAuth();
    const user = auth.currentUser;
    if (user != null) {
        const collectionNameUserRef = ref(db, `/data/${user.uid}/${cleansedName}`);
        await set(collectionNameUserRef, null);
    } else {
        console.error('user is ', user);
    }
  } catch (e) {
      console.error(e);
  }
}

export async function getCollectedBlob(collectionName: string, callback: any) {
  try {
    const cleansedName = collectionName.replace(/[\.\#\$\[\]]/g, '');
    const auth = getAuth();
    const user = auth.currentUser;
    if (user != null) {
        const collectionNameUserRef = ref(db, `/data/${user.uid}/${cleansedName}`);
        const snapshot = await get(collectionNameUserRef)

        if (snapshot.exists()) {
          const results = snapshot.val() ?? null;
          callback(results);
        } else {
            console.error('snapshot does not exist');
            return callback(null);
        }
    } else {
        console.error('user is ', user);
        return callback(null);
    }
  } catch (e) {
      console.error(e);
      callback(null);
  }
}

export async function getCollectedBlobNames(callback: any) {
  try {
    const auth = getAuth();
    if (auth != null) {
      const user = auth.currentUser;
      if (user != null) {
          const collectionNameUserRef = ref(db, `/files/${user.uid}`);
          const snapshot = await get(collectionNameUserRef)

          if (snapshot.exists()) {
            const results = snapshot.val();
            callback(results);
          } else {
              console.error('snapshot does not exist');
              return callback(null);
          }
      } else {
          console.error('user is ', user);
          return callback(null);
      }
    }
  } catch (e) {
      console.error(e);
      callback(null);
  }
}

export async function getCollectedBlobDataByName(collectionName: string, callback: any) {
  try {
    const cleansedName = collectionName.replace(/[\.\#\$\[\]]/g, '');
    const auth = getAuth();
    if (auth != null) {
      const user = auth.currentUser;
      if (user != null) {
          const collectionNameUserRef = ref(db, `/data/${user.uid}/${cleansedName}`);
          const snapshot = await get(collectionNameUserRef)

          if (snapshot.exists()) {
            const results = snapshot.val();
            callback(results);
          } else {
              console.error('snapshot does not exist');
              return callback(null);
          }
      } else {
          console.error('user is ', user);
          return callback(null);
      }
    }
  } catch (e) {
      console.error(e);
      callback(null);
  }
}
