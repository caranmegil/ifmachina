/*
Copyright (C) 2022  William Moore <william@nerderium.com>

This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of 
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License along with this program.  If not, see 
<http://www.gnu.org/licenses/>.
*/

import React from 'react';
import {
  Routes,
  Route,
} from "react-router-dom";

import './App.css';

import License from './components/License';
import GameLibrary from './components/GameLibrary';
import GameWindow from './components/GameWindow';

const App = () => {
  return (
    <div className="App" id="app">
      <Routes>
        <Route path="" element={<GameLibrary/>} />
        <Route path="/license" element={<License/>} />
        <Route path="story/:fileName" element={<GameWindow/>} />
      </Routes>
    </div>
  );
}

export default App;
