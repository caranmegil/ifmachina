/*

Common Parchment Options
========================

Copyright (c) 2022 Dannii Willis
MIT licenced
https://github.com/curiousdannii/parchment

*/

// import {WebGlkOte, Dialog, GlkOte, GlkOteOptions} from '../upstream/asyncglk/src/index-browser'
import {Dialog, GlkOte, GlkOteOptions} from '../upstream/asyncglk/src/index-common';

import {default as WebGlkOte} from '../upstream/asyncglk/src/glkote/web/web';

import WebDialog from '../upstream/glkote/dialog.js'

type ParchmentTruthy = boolean | number

export interface ParchmentOptions extends Partial<GlkOteOptions> {
    // Parchment options

    /** Whether or not to automatically launch Parchment */
    auto_launch?: ParchmentTruthy,
    /** Story path in the array format traditionally used by Parchment for Inform 7 */
    default_story?: [string],
    /** Domains to access directly: should always have both Access-Control-Allow-Origin and compression headers */
    direct_domains: string[],
    /** Path to resources */
    lib_path: string,
    /** URL of Proxy */
    proxy_url: string,
    /** Whether to load embeded resources in single file mode */
    single_file?: ParchmentTruthy,
    /** Story path */
    story?: string,
    /** Theme name, can be set to 'dark */
    theme?: string,
    /** Name of theme cookie to check */
    theme_cookie: string,
    /** Disable the file proxy, which may mean that some files can't be loaded */
    use_proxy?: ParchmentTruthy,

    // Modules to pass to other modules
    context_element?: HTMLElement | null,

    /** Dialog instance to use */
    Dialog: Dialog,
    /** GlkOte instance to use */
    GlkOte: GlkOte,

    // Common options for VMs

    /** Whether or not to load an autosave */
    do_vm_autosave?: ParchmentTruthy,
    clear_vm_autosave?: ParchmentTruthy,
}

export default function get_default_options(): ParchmentOptions {
    return {
        auto_launch: true,
        Dialog: WebDialog as any as Dialog,
        direct_domains: [
            ''
        ],
        single_file: false,
        do_vm_autosave: false,
        GlkOte: new WebGlkOte(),
        lib_path: 'dist/web',
        proxy_url: '',
        theme_cookie: 'parchment_theme',
        use_proxy: false,
        clear_vm_autosave: true,
        context_element: document.getElementById('app'),
    }
}