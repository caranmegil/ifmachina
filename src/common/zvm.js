// Export ZVM engine

import ZVM from 'ifvms/dist/zvm'
import ZVMDispatch from 'ifvms/src/zvm/dispatch'

export {ZVM, ZVMDispatch}