// Export Quixe engine

import * as QuixeModule from '../upstream/quixe/src/quixe/quixe.js'
import * as QuixeDispatch from '../upstream/quixe/src/quixe/gi_dispa.js'
import * as QuixeLoad from '../upstream/quixe/src/quixe/gi_load.js'

const GiDispa = QuixeDispatch.GiDispa;
const GiLoad = QuixeLoad.GiLoad;
const Quixe = QuixeModule.Quixe;

export {
  GiDispa,
  GiLoad,
  Quixe,
}